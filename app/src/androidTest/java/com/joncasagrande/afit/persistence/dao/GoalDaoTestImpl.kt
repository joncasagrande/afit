package com.joncasagrande.afit.persistence.dao

import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.joncasagrande.afit.persistence.dao.daoUtils.goal
import com.joncasagrande.afit.persistence.dao.daoUtils.goalList
import com.joncasagrande.afit.persistence.model.Goal
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException

class GoalDaoTestImpl : MainAbstractDao() {
    private lateinit var goalDao: GoalDao
    @Before
    override fun createDb() {
        super.createDb()

        goalDao = db.goalDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeGoalAndReadIn() {
        goalDao.insertGoal(goal)
        val goalId1 = goalDao.getGoalById(1)
        assertThat(goal.id, equalTo(goalId1.value!!.id))
    }

    @Test
    @Throws(Exception::class)
    fun writeListOfGoalsAndReadInList() {
        goalDao.insertGoals(goalList)
        val goalList = goalDao.getGoals()
        assertThat(goalList.value, not(emptyList<Goal>()))
    }

    @Test
    @Throws(Exception::class)
    fun writeListOfGoalsAndDeleteAll() {
        goalDao.insertGoals(goalList)
        goalDao.deleteGoals()
        val emptyGoalList = goalDao.getGoals()
        assertThat(emptyGoalList, equalTo(emptyGoalList))
    }

}