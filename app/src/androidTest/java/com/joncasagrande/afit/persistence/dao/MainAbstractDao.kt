package com.joncasagrande.afit.persistence.dao

import android.content.Context
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.joncasagrande.afit.persistence.AFitDataBase
import org.junit.Before

abstract class MainAbstractDao {

    protected lateinit var db: AFitDataBase
    lateinit var context: Context

    @Before
    open fun createDb() {
        context = InstrumentationRegistry.getInstrumentation().context
        db = Room.inMemoryDatabaseBuilder(
            context, AFitDataBase::class.java).build()
    }
}