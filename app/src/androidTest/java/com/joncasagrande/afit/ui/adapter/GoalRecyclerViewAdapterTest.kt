package com.joncasagrande.afit.ui.adapter

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.joncasagrande.afit.R
import com.joncasagrande.afit.persistence.dao.daoUtils.goalList
import com.joncasagrande.afit.persistence.dao.daoUtils.goalList1
import com.joncasagrande.afit.ui.MainActivity
import com.joncasagrande.afit.ui.fragments.GoalListFragment
import kotlinx.android.synthetic.main.fragment_goals_list.*
import org.junit.*

class GoalRecyclerViewAdapterTest() {

    @get:Rule
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    lateinit var instrumentationContext: Context
    lateinit var recyclerViewAdapter: GoalRecyclerViewAdapter
    lateinit var goalListFragment : GoalListFragment
    @Before
    fun setUp() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
        goalListFragment = GoalListFragment()
        activityTestRule.activity.supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameLayout,goalListFragment)
            .commit()

        goalListFragment.rv_goal.layoutManager = LinearLayoutManager(instrumentationContext)
        recyclerViewAdapter = GoalRecyclerViewAdapter(goalList.toMutableList())
        activityTestRule.activity.rv_goal.adapter = recyclerViewAdapter
    }

    @Test
    fun getItemCount() {
        Assert.assertEquals(goalList.size,recyclerViewAdapter!!.itemCount)
    }

    @Test
    fun swapData() {
        recyclerViewAdapter!!.swapData(goalList1)
        Assert.assertEquals(goalList1.size,recyclerViewAdapter!!.itemCount)
    }

    @Test
    fun onCreateViewHolder() {
    }


}