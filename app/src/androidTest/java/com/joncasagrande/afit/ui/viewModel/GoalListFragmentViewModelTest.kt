package com.joncasagrande.afit.ui.viewModel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.joncasagrande.afit.persistence.AFitDataBase
import com.joncasagrande.afit.persistence.dao.GoalDao
import com.joncasagrande.afit.persistence.dao.daoUtils.goal
import com.joncasagrande.afit.persistence.model.Goal
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.Collections.singletonList


class GoalListFragmentViewModelTest{

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    lateinit var database: AFitDataBase
    lateinit var dao: GoalDao
    lateinit var viewModel: GoalListFragmentViewModel
    lateinit var context : Context
    @Mock
    lateinit var observer: Observer<List<Goal>>
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        context =  InstrumentationRegistry.getInstrumentation().getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, AFitDataBase::class.java!!)
            .allowMainThreadQueries().build()
        dao = database.goalDao()
        viewModel = GoalListFragmentViewModel()
        viewModel.getGoals(context).observeForever(observer)

    }
    @After
    fun tearDown() {
        //database.close()
    }
    @Test
    fun getGoals() {
        // when
        dao.insertGoal(goal)
        viewModel.getGoals(context)
        // then
        Mockito.verify(observer).onChanged(singletonList(goal))

    }
}