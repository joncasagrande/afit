package com.joncasagrande.afit.rest


import io.reactivex.Single
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import retrofit2.http.Headers
import retrofit2.http.POST

class BaseApiTest {

    lateinit var baseAPI: BaseApi
    @Before
    fun setUp() {
        baseAPI = BaseApi()
    }

    @Test
    fun createService() {
        val testAPI: TestAPI =  baseAPI.createService(TestAPI::class.java)
        assertNotNull(testAPI)
    }


    interface TestAPI {

        @POST("data")
        @Headers("Content-type: application/json")
        fun getGoals(): Single<String>
    }
}