package com.joncasagrande.afit.rest

import org.junit.Before

import org.junit.Assert.*
import org.junit.Test

class BaseHttpTest {

    lateinit var baseHttp: BaseHttp
    @Before
    fun setUp() {
        baseHttp = BaseHttp()
    }

    @Test
    fun testClientNotNull(){
        assertNotNull(baseHttp.httpClient)
    }
    @Test
    fun testConectTimeout(){
        assertEquals(baseHttp.httpClient!!.connectTimeoutMillis(),60000)
    }

    @Test
    fun testReadTimeout(){
        assertEquals(baseHttp.httpClient!!.readTimeoutMillis(),60000)
    }

    @Test
    fun testInterceptor(){
        assertEquals(baseHttp.httpClient!!.interceptors().size,1)
    }

}