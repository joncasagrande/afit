package com.joncasagrande.afit.utils

import com.joncasagrande.afit.persistence.model.Goal
import com.joncasagrande.afit.persistence.model.Reward

val goal: Goal = Goal(1,"run","run forest run", "run", 10000, Reward("",1))
val goal1: Goal = Goal(2,"run","run for the mountain", "run", 1000, Reward("",1))
val goal2: Goal = Goal(3,"run","go go go ", "run", 5000, Reward("",1))
val goal3: Goal = Goal(4,"run","i am tired, I´ll go home", "walk", 5000, Reward("",1))

val goalList = listOf<Goal>(goal, goal1, goal2, goal3)

val goalList1 = listOf<Goal>(goal, goal1, goal2)