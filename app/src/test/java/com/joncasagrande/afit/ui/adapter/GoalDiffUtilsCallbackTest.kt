package com.joncasagrande.afit.ui.adapter

import com.joncasagrande.afit.utils.goalList
import com.joncasagrande.afit.utils.goalList1
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GoalDiffUtilsCallbackTest {

    lateinit var diffUtilsCallbackTest: GoalDiffUtilsCallback
    @Before
    fun setUp() {
        diffUtilsCallbackTest = GoalDiffUtilsCallback(goalList, goalList1)
    }


    @Test
    fun areItemsTheSame() {
        Assert.assertTrue(diffUtilsCallbackTest.areContentsTheSame(0,0))
    }
    @Test
    fun areItemsNOTTheSame() {
        Assert.assertFalse(diffUtilsCallbackTest.areContentsTheSame(0,1))
    }

    @Test
    fun getOldListSize() {
        Assert.assertEquals(goalList.size, diffUtilsCallbackTest.oldListSize)
    }

    @Test
    fun getNewListSize() {
        Assert.assertEquals(goalList1.size, diffUtilsCallbackTest.newListSize)

    }

    @Test
    fun areContentsTheSame() {
        Assert.assertTrue(diffUtilsCallbackTest.areContentsTheSame(0,0))
    }
    @Test
    fun areContentsNOTTheSame() {
        Assert.assertFalse(diffUtilsCallbackTest.areContentsTheSame(0,1))
    }
}