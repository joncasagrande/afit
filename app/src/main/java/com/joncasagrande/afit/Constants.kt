package com.joncasagrande.afit

const val zombieHand = "zombie_hand"
const val goldMedal = "gold_medal"
const val silverMedal = "silver_medal"
const val bronzeMedal = "bronze_medal"

const val goalParcelable = "goal"

const val step = "step"
const val walking = "walking_distance"
const val running = "running_distance"

