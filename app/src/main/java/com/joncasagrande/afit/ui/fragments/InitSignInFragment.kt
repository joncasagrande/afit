package com.joncasagrande.afit.ui.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.joncasagrande.afit.R
import kotlinx.android.synthetic.main.fragment_fit_signin.*

class InitSignInFragment : Fragment() {
    private val REQUEST_OAUTH_REQUEST_CODE = 10540

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_fit_signin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val fitnessOptions = FitnessOptions.builder()
            .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.AGGREGATE_SPEED_SUMMARY, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.AGGREGATE_ACTIVITY_SUMMARY, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)


            .build()
        if (GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this.context), fitnessOptions)) {
            changetoUserAchivementsFragment()
        }
        sign_in_button.setOnClickListener { loginGoogle(fitnessOptions) }
    }

    private fun changetoUserAchivementsFragment() {
        (getContext() as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.frameLayout, UserAchivementsFragment.newInstance())
            .commit()
    }

    private fun loginGoogle(fitnessOptions: FitnessOptions){
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this.context), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                this,
                REQUEST_OAUTH_REQUEST_CODE,
                GoogleSignIn.getLastSignedInAccount(this.context),
                fitnessOptions
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == RESULT_OK && requestCode == REQUEST_OAUTH_REQUEST_CODE){
                changetoUserAchivementsFragment()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    companion object {

        fun newInstance(): InitSignInFragment{
            return InitSignInFragment()
        }
    }
}