package com.joncasagrande.afit.ui.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.joncasagrande.afit.R
import com.joncasagrande.afit.goalParcelable
import com.joncasagrande.afit.persistence.model.Goal
import com.joncasagrande.afit.ui.GoalDetailsFragment
import kotlinx.android.synthetic.main.goal_item.view.*


class GoalRecyclerViewAdapter(var data: MutableList<Goal>) : RecyclerView.Adapter<GoalRecyclerViewAdapter.GoalViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalViewHolder {
        return GoalViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.goal_item, parent, false)
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: GoalViewHolder, position: Int) = holder.bind(data[position])

    fun swapData(goals: List<Goal>) {
        this.data = goals.toMutableList()
        notifyDataSetChanged()

        // compute diffs
        val diffCallback = GoalDiffUtilsCallback(this.data, goals)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        // clear contacts and add
        data.clear()
        data.addAll(goals)

        diffResult.dispatchUpdatesTo(this) // calls adapter's notify methods after diff is computed
    }

    inner class GoalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Goal) = with(itemView) {
            tv_title.text = item.title
            tv_description.text = item.description
            iv_badge.setImageDrawable(ContextCompat.getDrawable(itemView.context, item.reward.getDrawable()))

            setOnClickListener {
                val goalDetailsFragment = GoalDetailsFragment.newInstance()
                val arguments  = Bundle()
                arguments.putParcelable(goalParcelable, item)
                goalDetailsFragment.arguments = arguments

                (it.getContext() as FragmentActivity).supportFragmentManager.beginTransaction()
                    .addSharedElement(iv_badge, ViewCompat.getTransitionName(iv_badge)!!)
                    .addToBackStack("GOALS")
                    .replace(R.id.frameLayout, goalDetailsFragment)
                    .commit()

            }
        }
    }
}