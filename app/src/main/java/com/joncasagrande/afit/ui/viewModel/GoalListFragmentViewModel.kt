package com.joncasagrande.afit.ui.viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.joncasagrande.afit.persistence.AFitDataBase
import com.joncasagrande.afit.persistence.dao.GoalDao
import com.joncasagrande.afit.persistence.model.Goal

class GoalListFragmentViewModel: ViewModel() {
    lateinit var goaldao : GoalDao

    fun getGoals(context: Context): LiveData<List<Goal>> {
        if(!::goaldao.isInitialized){
            goaldao = AFitDataBase.getInstance(context).goalDao()
        }
        return goaldao.getGoals()
    }
}