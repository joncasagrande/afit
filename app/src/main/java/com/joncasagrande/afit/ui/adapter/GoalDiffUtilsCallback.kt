package com.joncasagrande.afit.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.joncasagrande.afit.persistence.model.Goal

class GoalDiffUtilsCallback(val oldList: List<Goal>, val newList: List<Goal>): DiffUtil.Callback(){

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val newGoal = newList[newItemPosition]
        val oldGoal = oldList[oldItemPosition]

        return newGoal.title == oldGoal.title
                && newGoal.description == oldGoal.description
                && newGoal.reward.getDrawable() == oldGoal.reward.getDrawable()
    }

}