package com.joncasagrande.afit.ui.viewModel

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.joncasagrande.afit.persistence.AFitDataBase
import com.joncasagrande.afit.persistence.model.GoalsRest
import com.joncasagrande.afit.rest.repo.GoalsRepo
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync

class MainActivityViewModel : ViewModel() {

    fun getGoalRepo(context: Context){
        val goalsRepo = GoalsRepo(object : GoalsRepo.Callback{
            override fun getDisposable(d: Disposable) {

            }

            override fun onSuccess(goalRest: GoalsRest) {
                Log.d("", "deu certo")
                doAsync {
                    val goalDao = AFitDataBase.getInstance(context).goalDao()
                    goalDao.insertGoals(goalRest.items)
                }
            }

            override fun onError(message: String) {
                Log.d("", "deu errado")
            }
        })
        goalsRepo.getGoals()
    }
}