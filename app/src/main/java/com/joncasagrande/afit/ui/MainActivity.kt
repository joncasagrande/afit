package com.joncasagrande.afit.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.joncasagrande.afit.R
import com.joncasagrande.afit.ui.fragments.InitSignInFragment
import com.joncasagrande.afit.ui.viewModel.MainActivityViewModel


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = MainActivityViewModel()
        viewModel.getGoalRepo(this)


        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameLayout, InitSignInFragment.newInstance())
        ft.commit()
    }
}
