package com.joncasagrande.afit.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import com.joncasagrande.afit.goalParcelable
import com.joncasagrande.afit.persistence.model.Goal
import kotlinx.android.synthetic.main.fragment_goal_detail.*


class GoalDetailsFragment : Fragment() {
    var goal :Goal? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        goal = arguments!!.getParcelable<Goal>(goalParcelable)
        return inflater.inflate(com.joncasagrande.afit.R.layout.fragment_goal_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if(goal != null) {
            tv_title.text = goal!!.title
            tv_description.text = goal!!.description
            iv_badge.setImageResource(goal!!.reward.getDrawable())
            tv_type_goal.text = getString(com.joncasagrande.afit.R.string.type_goal,goal!!.type.replace("_", " ").capitalize() , goal!!.goal.toString())
            iv_type.setImageResource(goal!!.getTypeDrawable())
            tv_points.text = getString(com.joncasagrande.afit.R.string.points,goal!!.reward.points.toString())
        }
    }

    companion object {

        fun newInstance(): GoalDetailsFragment {
            return GoalDetailsFragment()
        }
    }
}