package com.joncasagrande.afit.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.joncasagrande.afit.R
import com.joncasagrande.afit.persistence.model.Goal
import com.joncasagrande.afit.ui.adapter.GoalRecyclerViewAdapter
import com.joncasagrande.afit.ui.viewModel.GoalListFragmentViewModel
import kotlinx.android.synthetic.main.fragment_goals_list.*

class GoalListFragment : Fragment(){

    var adatper : GoalRecyclerViewAdapter? = null
    lateinit var viewModel : GoalListFragmentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(this).get(GoalListFragmentViewModel::class.java)
        return inflater.inflate(R.layout.fragment_goals_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv_goal.layoutManager = LinearLayoutManager(context)

        val goalListObserver = Observer<List<Goal>> { goalsList ->
            if(adatper!=null){
                adatper!!.swapData(goalsList)
            }else{
                adatper = GoalRecyclerViewAdapter(goalsList.toMutableList())
            }
            rv_goal.adapter = adatper
        }
        viewModel.getGoals(context!!).observe(this, goalListObserver)
    }

    companion object {

        fun newInstance(): GoalListFragment {
            return GoalListFragment()
        }
    }
}