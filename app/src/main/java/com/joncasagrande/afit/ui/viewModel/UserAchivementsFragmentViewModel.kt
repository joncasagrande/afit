package com.joncasagrande.afit.ui.viewModel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessActivities
import com.google.android.gms.fitness.data.DataPoint
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.result.DataReadResult
import com.joncasagrande.afit.persistence.AFitDataBase
import com.joncasagrande.afit.persistence.dao.GoalDao
import com.joncasagrande.afit.persistence.model.Goal
import org.jetbrains.anko.doAsync
import java.util.*
import java.util.concurrent.TimeUnit

class UserAchivementsFragmentViewModel() : ViewModel() {

    val TAG = UserAchivementsFragmentViewModel::class.java.simpleName
    lateinit var client : GoogleApiClient
    lateinit var goaldao : GoalDao

    val walkingObservable: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
    val runningObservable: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    val bikingObservable: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    fun getGoals(context: Context): LiveData<List<Goal>> {

        if(!::goaldao.isInitialized){
            goaldao = AFitDataBase.getInstance(context).goalDao()
        }
        return goaldao.getGoals()
    }

    fun createGoogleClient(context: Context){
        client = GoogleApiClient.Builder(context)
            .addApi(Fitness.HISTORY_API)
            .build()
        client.connect()
    }

    fun getSteps(): LiveData<Int>{
        doAsync {
            var total: Long = 0

            val result = Fitness.HistoryApi.readDailyTotal(client, DataType.TYPE_STEP_COUNT_DELTA)
            val totalResult = result.await(30, TimeUnit.SECONDS)
            if (totalResult.getStatus().isSuccess()) {
                val totalSet = totalResult.getTotal()
                total = (if (totalSet!!.isEmpty())
                    0
                else
                    totalSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt()).toLong()
            } else {
                Log.w(TAG, "There was a problem getting the step count.")
            }

            Log.i(TAG, "Total steps: $total")
            walkingObservable.postValue(total.toInt())

        }
        return walkingObservable
    }

    fun getActivitiesAgregate() {
        doAsync {
            val yesterday = GregorianCalendar.getInstance()
            yesterday.add(Calendar.DATE, -1)

            val today = GregorianCalendar.getInstance()
            val readRequest: DataReadRequest = DataReadRequest.Builder()
                .aggregate(DataType.TYPE_ACTIVITY_SEGMENT, DataType.AGGREGATE_ACTIVITY_SUMMARY)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(yesterday.timeInMillis, today.timeInMillis, TimeUnit.MILLISECONDS)
                .build()

            val pendingResult = Fitness.HistoryApi.readData(client, readRequest)

            val dataReadResult = pendingResult.await(60, TimeUnit.SECONDS)
            Log.d(TAG, "Retrieved results: $dataReadResult")

            if (dataReadResult.status.isSuccess) {
                retrieveHistoryData(dataReadResult)
            } else {
                Log.w(TAG, "There was a problem getting the step count.")
            }
        }
    }

    private fun retrieveHistoryData(dataReadResult: DataReadResult) {
        var walking = 0
        var running = 0
        var longDistances = 0

        dataReadResult.buckets.forEach {
            val dataSets = it.dataSets
            dataSets.forEach{
                it.dataPoints.forEach {
                    walking += getBucketData(it, FitnessActivities.WALKING)
                    running += getBucketData(it, FitnessActivities.RUNNING)
                    longDistances += getBucketData(it, FitnessActivities.AEROBICS)
                }
            }
        }

        walkingObservable.postValue(walking)
        runningObservable.postValue(running)
        bikingObservable.postValue(longDistances)
        Log.d(TAG, "walking: $walking")
        Log.d(TAG, "running: $running")
        Log.d(TAG, "biking : $longDistances")
    }

    private fun getBucketData(dp: DataPoint, activityName: String): Int {
        var count = 0
        for (field in dp.getDataType().getFields()) {
            if ("activity" == field.getName()){
                count += dp.getValue(field).asInt()
            }
        }
        return count
    }


    fun getAchievmentBaseOnProgress(achivement: Int, goals : List<Goal>):Int{
        var prevGoal = Int.MIN_VALUE
        var biggerGoal = achivement
        var badge = 0
        goals.forEach {
            if(it.goal > prevGoal && it.goal <= biggerGoal){
                prevGoal = it.goal
                badge = it.getTypeDrawable()
            }
        }
        return badge
    }
}