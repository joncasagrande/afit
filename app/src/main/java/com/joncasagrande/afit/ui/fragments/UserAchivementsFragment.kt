package com.joncasagrande.afit.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.joncasagrande.afit.R
import com.joncasagrande.afit.persistence.model.Goal
import com.joncasagrande.afit.running
import com.joncasagrande.afit.step
import com.joncasagrande.afit.ui.viewModel.UserAchivementsFragmentViewModel
import com.joncasagrande.afit.walking
import kotlinx.android.synthetic.main.fragment_user_achivements.*

class UserAchivementsFragment : Fragment(){


    lateinit var viewModel: UserAchivementsFragmentViewModel
    lateinit var goalList : List<Goal>

    val stepArray = mutableListOf<Int>()
    val runningArray = mutableListOf<Int>()
    val walkingArray = mutableListOf<Int>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(this).get(UserAchivementsFragmentViewModel::class.java)
        viewModel.createGoogleClient(context!!)


        return inflater.inflate(R.layout.fragment_user_achivements, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        floatingActionButton.setOnClickListener({openGoalListFragment()})


        val walkObserver = Observer<Int> { steps ->
            val tryOut = 1500
            walkingProgress.currentProgress = tryOut
            if(walkingProgress.totalProgress < tryOut){
                walkingProgress.totalProgress = (tryOut * 1.1).toInt()
            }
            val badge = viewModel.getAchievmentBaseOnProgress(tryOut,goalList.filter { it.type == step })
            if(badge != 0) {
                walking_achivement_imageview.setImageDrawable(ContextCompat.getDrawable(this@UserAchivementsFragment.context!!, R.drawable.ic_hand))
            }

        }

        val runningObserver = Observer<Int> { run ->
            runningProgress.currentProgress = run

        }

        val bikingObserver = Observer<Int> { bike ->
            bikingProgress.currentProgress = bike

        }

        val goalListObserver = Observer<List<Goal>> { goalsList ->
            this@UserAchivementsFragment.goalList = goalsList
            goalsList.forEach {
                when(it.type){
                    step -> stepArray.add(it.goal)
                    running -> stepArray.add(it.goal)
                    walking -> stepArray.add(it.goal)
                }
            }
            walkingProgress.markers = stepArray
            runningProgress.markers = runningArray
            bikingProgress.markers = walkingArray
        }
        viewModel.getGoals(context!!).observe(this, goalListObserver)

        floatingActionButton.setOnClickListener({openGoalListFragment()})

        viewModel.getSteps().observe(this, walkObserver)

      //  viewModel.getACtivities()
      //  viewModel.getActivitiesAgregate()

    }

    private fun openGoalListFragment(){
        (getContext() as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.frameLayout, GoalListFragment.newInstance())
            .addToBackStack("UserAchivementsFragment")
            .commit()
    }

    companion object {

        fun newInstance(): UserAchivementsFragment{
            return UserAchivementsFragment()
        }
    }
}