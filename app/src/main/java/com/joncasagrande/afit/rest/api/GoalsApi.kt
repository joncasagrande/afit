package com.joncasagrande.afit.rest.api

import com.joncasagrande.afit.persistence.model.GoalsRest
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface GoalsApi {
    @GET("goals")
    @Headers("Content-type: application/json")
    fun getGoals(): Single<GoalsRest>
}