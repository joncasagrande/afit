package com.joncasagrande.afit.rest

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
class BaseHttp {
    var httpClient: OkHttpClient? = null


    private val logginInterceptor: HttpLoggingInterceptor
        get() {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return loggingInterceptor
        }

    init {
        if (httpClient == null)
            httpClient = createHttpClient()
    }

    private fun createHttpClient(): OkHttpClient {
        val httpClientBuilder = OkHttpClient().newBuilder()
        httpClientBuilder.connectTimeout(60L, TimeUnit.SECONDS)
        httpClientBuilder.readTimeout(60L, TimeUnit.SECONDS)
        httpClientBuilder.addInterceptor(logginInterceptor)
        return httpClientBuilder.build()
    }
}