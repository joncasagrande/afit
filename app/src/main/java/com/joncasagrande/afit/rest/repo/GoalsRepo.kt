package com.joncasagrande.afit.rest.repo

import com.joncasagrande.afit.persistence.model.GoalsRest
import com.joncasagrande.afit.rest.BaseApi
import com.joncasagrande.afit.rest.api.GoalsApi
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class GoalsRepo(val callback: Callback) {
    val baseAPI : BaseApi = BaseApi()
    private val goalsApi: GoalsApi = baseAPI.createService(GoalsApi::class.java)

    interface Callback {
        fun onSuccess(goalRest: GoalsRest)
        fun getDisposable(d: Disposable)
        fun onError(message: String)
    }

    fun getGoals() {
        val get = goalsApi.getGoals()
        get.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<GoalsRest>{
                override fun onSuccess(goalRest: GoalsRest) {
                    callback.onSuccess(goalRest)
                }

                override fun onSubscribe(d: Disposable) {
                    callback.getDisposable(d)
                }

                override fun onError(e: Throwable) {
                    var errorMEssage = "Fail to get goals!"
                    if(e.message !=null) {
                        errorMEssage = e.message!!
                    }
                    callback.onError(errorMEssage)
                }
            })
    }
}