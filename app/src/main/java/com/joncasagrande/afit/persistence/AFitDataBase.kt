package com.joncasagrande.afit.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.joncasagrande.afit.persistence.dao.GoalDao
import com.joncasagrande.afit.persistence.dao.RewardDao
import com.joncasagrande.afit.persistence.model.Goal
import com.joncasagrande.afit.persistence.model.Reward

@Database(entities = arrayOf(Goal::class, Reward::class), version = 1)
abstract class AFitDataBase : RoomDatabase() {

    abstract fun goalDao() : GoalDao
    abstract fun rewardDao() : RewardDao

    companion object {

        @Volatile private var INSTANCE: AFitDataBase? = null

        fun getInstance(context: Context): AFitDataBase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context,
                AFitDataBase::class.java, "AFit.db")
                .build()
    }
}