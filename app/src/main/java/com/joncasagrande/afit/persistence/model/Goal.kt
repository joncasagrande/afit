package com.joncasagrande.afit.persistence.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.joncasagrande.afit.R
import kotlinx.android.parcel.Parcelize

/*
*
* {
   "id": "1000",
   "title": "Easy walk steps",
   "description": "Walk 500 steps a day",
   "type": "step",
   "goal": 500,
   "reward": {
    "trophy": "bronze_medal",
    "points": 5
   }
  }
*
*/
@Entity
@Parcelize
data class Goal(@PrimaryKey val id: Int,
                val title: String,
                val description : String,
                val type: String,
                val goal: Int,
                @Embedded val reward: Reward): Parcelable{
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Goal

        if (id != other.id) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (type != other.type) return false
        if (goal != other.goal) return false
        if (reward != other.reward) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + title.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + goal
        result = 31 * result + reward.hashCode()
        return result
    }

    fun getTypeDrawable(): Int{
        return when(type){
            "step" -> R.drawable.ic_pedestrian_walking
            "walking_distance" -> R.drawable.ic_walking_distance
            "running_distance" -> R.drawable.ic_running
            else -> R.drawable.ic_standing
        }
    }
}



