package com.joncasagrande.afit.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.joncasagrande.afit.persistence.model.Goal
import com.joncasagrande.afit.persistence.model.Reward
import io.reactivex.Completable

import io.reactivex.Flowable


/**
 * Data Access Object for the users table.
 */
@Dao
interface RewardDao {


    /**
     * Get a goal by id.
     * @return the goal from the table with a specific id.
     */
    @Query("SELECT * FROM Reward WHERE trophy = :trophy")
    fun getGoalByTrophy(trophy: String): Reward

    /**
     * Insert a goal in the database. If the user already exists, replace it.
     * @param user the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertReward(reward: Reward)

    /**
     * Delete all users.
     */
    @Query("DELETE FROM Reward")
    fun deleteRewards()
}