package com.joncasagrande.afit.persistence.model


data class GoalsRest(val items: List<Goal>, val nextPageToken: String)
