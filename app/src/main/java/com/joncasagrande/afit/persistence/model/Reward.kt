package com.joncasagrande.afit.persistence.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.joncasagrande.afit.*
import kotlinx.android.parcel.Parcelize
import java.util.*

/*
 * "reward": {
    "trophy": "bronze_medal",
    "points": 5
   }
 */
@Entity
@Parcelize
data class Reward(@PrimaryKey val trophy: String, val points: Int): Parcelable{

    fun getDrawable(): Int {
        return when(trophy){
            goldMedal -> R.drawable.ic_gold_medal
            silverMedal -> R.drawable.ic_silver_medal
            bronzeMedal -> R.drawable.ic_bronse_medal
            zombieHand -> R.drawable.ic_hand
            else -> R.drawable.ic_launcher_foreground

        }
    }
}
