package com.joncasagrande.afit.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.joncasagrande.afit.persistence.model.Goal


/**
 * Data Access Object for the users table.
 */
@Dao
interface GoalDao {

    /**
     * Get a list of goals .
     * @return the list of goals from the table.
     */
    @Query("SELECT * FROM Goal")
    fun getGoals(): LiveData<List<Goal>>

    /**
     * Get a goal by id.
     * @return the goal from the table with a specific id.
     */
    @Query("SELECT * FROM Goal WHERE id = :id")
    fun getGoalById(id: Int): LiveData<Goal>


    /**
     * Get a goal by type.
     * @return the goal from the table with a specific id.
     */
    @Query("SELECT * FROM Goal WHERE type = :type")
    fun getGoalByType(type: String): LiveData<Goal>

    /**
     * Insert a goal in the database. If the user already exists, replace it.
     * @param goal the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGoal(goal: Goal)


    /**
     * Insert a goal in the database. If the user already exists, replace it.
     * @param goalList the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGoals(goalList: List<Goal>)

    /**
     * Delete all users.
     */
    @Query("DELETE FROM Goal")
    fun deleteGoals()
}